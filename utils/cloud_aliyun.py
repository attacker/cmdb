
# aliyun-python-sdk-ecs==4.24.13
from aliyunsdkcore.client import AcsClient
from aliyunsdkecs.request.v20140526.DescribeRegionsRequest import DescribeRegionsRequest
from aliyunsdkecs.request.v20140526.DescribeZonesRequest import DescribeZonesRequest
from aliyunsdkecs.request.v20140526.DescribeInstancesRequest import DescribeInstancesRequest
from aliyunsdkecs.request.v20140526.DescribeDisksRequest import DescribeDisksRequest
import json
import sys

class AliCloud():
    def __init__(self, secret_id, secret_key):
        self.secret_id = secret_id
        self.secret_key = secret_key

    # 地区
    def region_list(self):
        client = AcsClient(self.secret_id, self.secret_key)
        req = DescribeRegionsRequest()
        try:
            res = client.do_action_with_exception(req)
            data = json.loads(res.decode())
            return {'code': 200, 'data': data}
        except Exception as e:
            return {'code': 500, 'msg': e.get_error_msg()}

    # 可用区
    def zone_list(self, region_id):
        client = AcsClient(self.secret_id, self.secret_key)
        req = DescribeZonesRequest()
        req.add_query_param('RegionId', region_id)
        try:
            res = client.do_action_with_exception(req)
            data = json.loads(res.decode())
            return {'code': 200, 'data': data}
        except Exception as e:
            return {'code': 500, 'msg': e.get_error_msg()}

    ## 实例
    def instance_list(self, region_id):
        client = AcsClient(self.secret_id, self.secret_key)
        req = DescribeInstancesRequest()
        req.add_query_param('RegionId', region_id)
        req.add_query_param('PageSize',10) ##  单页条数
        try:
            next = 0
            instances = []
            for i in range(1, 100): 
                req.set_PageNumber(i)
                result = json.loads(client.do_action_with_exception(req))
                curInstances =  result.get('Instances').get('Instance')
                TotalCount = result['TotalCount']
                instances = instances + curInstances
                if next == 1:
                    break
                if not result['NextToken']:  
                    next = 1 
            print("instances len:",len(instances),"TotalCount:",TotalCount)
            return {'code': 200, 'data': instances}
        except Exception as e:
            return {'code': 500, 'msg': e}


    # 实例关联的硬盘
    def instance_disk(self, instance_id):
        client = AcsClient(self.secret_id, self.secret_key)
        req = DescribeDisksRequest()
        req.add_query_param('InstanceId', instance_id)
        try:
            res = client.do_action_with_exception(req)
            data = json.loads(res.decode())
            return {'code': 200, 'data': data}
        except Exception as e:
            return {'code': 500, 'msg': e.get_error_msg()}

if __name__ == '__main__':
    accesskey = sys.argv[1]
    secret = sys.argv[2]
    cloud = AliCloud( accesskey, secret)
    result = cloud.region_list()
    print(result)
    # result = cloud.zone_list('cn-beijing')
    cloud.instance_list('cn-shanghai')
    # result = cloud.instance_list('cn-beijing')
    # ['Instances']['Instance']
    # TotalCount = result['data']['TotalCount']
    # print( f"Total: {TotalCount}" )
    # for i in result['data']['Instances']['Instance']:
    #     print(i['InstanceName'])
    # sdk 分页遍历
      
    # result = cloud.instance_disk('i-2zeffuf3e3uixm96jabc')
    # print(result)


