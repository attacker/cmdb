'''
Author: admin@attacker.club
Date: 2022-07-24 11:05:26
LastEditTime: 2022-07-24 11:50:46
Description: 
'''
import git
import subprocess


def get_branch_from_url(git_url):
    """
    不拉代码判断远程仓库的有哪些分支
    :param git_url:
    :return:
    """
    branches = set()
    cmd = f"git ls-remote -h {git_url}"
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    p.wait()
    output = p.stdout.read().decode("utf-8").split("\n")
    # output = subprocess.check_output(cmd, shell=True).decode("utf-8").split("\n")
    for line in output:
        if line is not None and line != "":
            branch = line.split("heads/")[1]
            branches.add(branch)
            # print(branch)
    return branches


def getGitFile():
        import git
        repo = git.Repo.init()
        repo = git.Repo.clone_from(url='https://gitee.com/attacker/cmdb.git', to_path='./test')
        print(repo,repo.active_branch)
        branchs = []
        for ref in repo.git.branch('-r').split('\n'):
            print(ref)
            branchs.append(ref)
        print(branchs)

# https://gitee.com/attacker/devops.git
# git@gitee.com:attacker/devops.git
# getGitFile()
# s = get_branch_from_url("https://gitee.com/attacker/cmdb.git")
# print(s)


