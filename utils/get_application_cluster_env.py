#!/usr/bin/env python
# coding=utf-8
# 基于应用名获取主机地址
# python cluster.py pro-blog

import requests
import json
import sys

def getToken(url,data):
    ret = requests.post(url=url, json=data) 
    if ret.status_code == 200 and 'token' in ret.json()['data']:
        token = ret.json()['data']['token']
    else:
        token = Null
    return token

def search_API(*args, **kwargs):

    headers = {
            'Authorization': 'Bearer {}'.format(getToken(args[0],data=kwargs['data'])),
    }
    url = '{0}{1}'.format(args[1],kwargs['name'])
    ret =  requests.get(url=url,headers=headers ) 
    hosts = ret.json()['data']['results']
    results = hosts[0]['env_id']
    return results




if __name__ == '__main__':
    URL = 'http://127.0.0.1:8000'
    Login_API = '{}/api/oauth/login/'.format(URL)
    cluster_API = '{}/api/app/applications/?&search='.format(URL)
    
    data = {
    "username":"admin",
    "password": "123456"
    }

    name = sys.argv[1]  # 'pro-blog'
    results = search_API(Login_API,cluster_API,data=data,name=name)
    print(results)