
# pip install aliyun-python-sdk-bssopenapi
from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.acs_exception.exceptions import ClientException
from aliyunsdkcore.acs_exception.exceptions import ServerException
from aliyunsdkcore.auth.credentials import AccessKeyCredential
from aliyunsdkcore.auth.credentials import StsTokenCredential
from aliyunsdkbssopenapi.request.v20171214.QueryAccountBalanceRequest import QueryAccountBalanceRequest
import json
import sys

class AliCloud():
    def __init__(self, secret_id, secret_key):
        self.secret_id = secret_id
        self.secret_key = secret_key
        self.credentials = AccessKeyCredential(self.secret_id ,self.secret_key)

    def queryAccountBalance(self):
        """ 余额查询 """

        client = AcsClient(region_id='cn-shanghai', credential=self.credentials)
        req = QueryAccountBalanceRequest()
        req.set_accept_format('json')
        try:
            res = client.do_action_with_exception(req)
            data = json.loads(res.decode())
            return {'code': 200, 'data': data}
        except Exception as e:
            return {'code': 500, 'msg': e.get_error_msg()}
   

if __name__ == '__main__':
    accesskey = sys.argv[1]
    secret = sys.argv[2]
    cloud = AliCloud( accesskey, secret)
    result = cloud.queryAccountBalance()
    res = result['data']['Data']['AvailableCashAmount']
    cost = float(res.replace(',',''))
    print(cost)