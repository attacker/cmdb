
#!/usr/bin/env python
# coding=utf-8

import os
import shlex

class Initialize(object):

    def bash(self,cmd):
        """
        执行bash命令
        """
        shlex.os.system(cmd)
    
    def run(self):
        self.bash('python manage.py makemigrations oauth asset app')
        self.bash('python manage.py migrate')

        try:
            import django
            os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'website.settings')
            django.setup()
            # 初始化
            from django.contrib.auth import get_user_model
            UserProfile = get_user_model()
            users = UserProfile.objects.all()

            if len(users) == 0: # 如果没有用户就初始化数据
                # User.objects.create_superuser(
                #     username="admin", password="123456", email="admin@qq.com")
                self.import_data()
        except Exception as e:
            pass

    def import_data(self):
        """
        导入默认配置
        """
        self.bash('python manage.py loaddata doc/dumpdata.json')
    #     from django.db import connection  # UPDATE,DELETE等SQL语句操作
    #     with connection.cursor() as cursor:
    #         with open("./update.sql") as f:
    #             for raw in f:
    #                 raw = raw.strip()
    #                 if raw:
    #                     cursor.execute(raw.strip())  # 执行sql
   
        
if __name__ == '__main__':
    Init = Initialize()
    Init.run()
