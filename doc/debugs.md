<!--
 * @Author: admin@attacker.club
 * @Date: 2022-07-19 22:39:07
 * @LastEditTime: 2022-07-19 22:59:54
 * @Description:
-->

# 调试

## 调试 shell

```bash
python manage.py shell
import os
import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'website.settings')
django.setup()

from asset.models import Host
Host.objects.all()
```

## channel_layer
```
python manage.py shell

from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
channel_layer = get_channel_layer()

async_to_sync(channel_layer.send)(
    'test',{
        'site2':"www1"
        }
)
# 发送消息
async_to_sync(channel_layer.receive)('test')
# 接受消息
```

## Mac 安装 python-ldap

如果不想安装 xcode,那么可以通过重置系统默认开发工具路径
from asgiref.sync import async_to_sync


```bash
sudo xcode-select -r
xcode-select --switch /Library/Developer/CommandLineTools
xcode-select -p

cd python-ldap-3.4.2
python setup.py install
```
