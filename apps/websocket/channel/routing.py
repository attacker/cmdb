from django.urls import path,re_path
from websocket.channel import Consumer

websocket_urlpatterns = [
    path('webssh/', Consumer.SSHConsumer),
    re_path('ansible/', Consumer.AnsibleConsumer),
]