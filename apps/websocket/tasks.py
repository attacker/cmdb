from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

from celery import shared_task
from website.celeryCenter import app as celeryApp
# from websocket.ssh import SSH
# async_to_sync(channel_layer.send)('test_channel', {'type': 'hello'})
# async_to_sync(channel_layer.receive)('test_channel')
from utils.custom_log import log_start
logger = log_start('task')

@shared_task
def task_exec_add(hosts=None,groups=None,channel_name=None):
    print(f"Task:{hosts} {groups}")
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.send)(channel_name,{
        "type": "send.message",
        "message": 'xxxxxx'})
  

    # # 通过channels_layers从websocketconsumer函数外部向channels发送消息，
    # channel_layer = get_channel_layer()
    # # 异步转同步使用async_to_sync方法，channel_name为频道名，这里直接用的websocketconsumer自带的channel_name属性，type发送的方法，send.message对应前面MyConsumer里的send_message，这里channels是对.进行了内部转化为_，没啥特殊理解的，message就是你要发送的消息
    # async_to_sync(channel_layer.send)(
    # channel_name,
    # {"type": "send.message","message": ''}
    #)