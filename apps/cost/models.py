from django.db import models

# Create your models here.


# class Account(models.Model):
#     """ 平台账号 """
#     name = models.CharField(max_length=255, blank=True, null=True, verbose_name="账户")
#     platform_id = models.ForeignKey("Platform", db_column='platform_id',  blank=True, null=True, on_delete=models.SET_NULL)
#     accesskey = models.CharField( max_length=64,default='', verbose_name='AccessKey ID')
#     secret = models.CharField( max_length=64,default='', verbose_name='Secret ID')
#     gmt_create = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
#     gmt_modified = models.DateTimeField(auto_now=True, verbose_name="编辑时间")
#     desc =  models.CharField(max_length=50, blank=True, null=True, verbose_name="描述")
#     class Meta:
#         # db_table = "cmdb_asset_platform_account"
#         verbose_name = '平台账号'
#         verbose_name_plural = verbose_name
#         ordering = ['id']
#         # unique_together = (('accesskey', 'secret'),)
#     def __str__(self):
#         return self.name