
from asset.models import *
from api.serializers.asset_group import *
from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter, OrderingFilter

class GroupSerializerViewSet(ModelViewSet):
    """
    create:
    平台账号--新增

    平台账号新增, status: 201(成功), return: 新增平台账号信息

    destroy:
    平台账号--删除

    平台账号删除, status: 204(成功), return: None

    update:
    平台账号--修改

    平台账号修改, status: 200(成功), return: 修改后的平台账号信息

    partial_update:
    平台账号--局部修改(平台账号授权)

    平台账号局部修改, status: 200(成功), return: 修改后的平台账号信息

    list:
    平台账号--获取列表

    平台账号列表信息, status: 200(成功), return: 平台账号信息列表
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('name', 'desc') # &search=host-2
    ordering_fields = ('id', 'name')

