from django.urls import path,re_path, include
from oauth.views import oauth
from oauth.views import personal
from oauth.views import departments
from oauth.views import permissions
from oauth.views import roles


from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import refresh_jwt_token



app_name = "api"
urlpatterns = [ 

    path('oauth/refresh', refresh_jwt_token), # 刷新token
    path('oauth/login/', oauth.UserLoginView.as_view()), # 账号登录
    path('oauth/userinfo/', oauth.UserInfoView.as_view()), #  获取用户信息
    path('oauth/logout/', oauth.LogoutAPIView.as_view()), # 账号登出

    path('information/change-password/', personal.ChangePasswordAPIView.as_view()),  # 修改个人密码
    path('information/change-information/', personal.ChangeInformationAPIView.as_view()),  # 修改个人信息
    path('information/change-avatar/', personal.ChangeAvatarAPIView.as_view()),  # 修改个人头像


    path('user/', include('oauth.urls')), # 授权管理
    path('asset/', include('asset.urls')), # 资产管理
    path('app/', include('app.urls')), # 应用管理
    path('monitor/', include('monitor.urls')), # monitor

   
]
