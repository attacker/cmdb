
from rest_framework import serializers
from asset.models import *
from .mixins  import *
import json



class PlatformSerializer(serializers.ModelSerializer):
    
    gmt_create = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S', read_only=True)
    gmt_modified = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S', read_only=True)


    class Meta:
        model = Platform
        fields = "__all__"  


class PlatformAccountSerializer(serializers.ModelSerializer,AssetMixin):
    
    gmt_create = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S', read_only=True)
    gmt_modified = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S', read_only=True)

    def set_validated_data(self,validated_data):
        if "platform_id" in self.initial_data.keys():
            validated_data['platform_id'] = self.get_platform_id()
        return validated_data

    def create(self, validated_data):
        validated_data = self.set_validated_data(validated_data)
        return super().create(validated_data)
        
    def update(self,instance,validated_data):
        validated_data = self.set_validated_data(validated_data)
        return super().update(instance, validated_data)

    class Meta:
        model = PlatformAccount
        fields = "__all__"
        depth = 1
        # depth = 2