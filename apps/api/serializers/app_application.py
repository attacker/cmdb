from rest_framework import serializers
from app.models import *
from .mixins  import *
import json
# from asset.models import Host
# from api.serializers.hosts import Hostserializer

class ApplicationSerializer(serializers.ModelSerializer,AssetMixin):
    """ 应用信息 """
    
    # def set_validated_data(self,validated_data):
        # if "host_id" in self.initial_data.keys():
        #     validated_data['host_id'] = self.get_host_id()
        # if "prod_id" in self.initial_data.keys():
        #     validated_data['prod_id'] = self.get_prod_id()

        # if "env_id" in self.initial_data.keys():
        #     validated_data['env_id'] = self.get_env_id()
        
        # if "host_id" in self.initial_data.keys():
        #     validated_data['host_id'] = self.get_host_id()
        
        # return validated_data

    # def create(self, validated_data):
    #     validated_data = self.set_validated_data(validated_data)
    #     return super().create(validated_data)
          
    # def update(self,instance,validated_data):
    #     validated_data = self.set_validated_data(validated_data)
    #     return super().update(instance, validated_data)
    class Meta:
        model = Application
        fields = "__all__"

    
    def to_representation(self, instance):
        """自定义字段返回 """
        data = super().to_representation(instance)
        # data['app_app'] = instance.get_env_id()
        # isinstance(item.get("department_id")
        data['app_env'] = EnvConfigSerializer(instance.env_id).data
        data['app_prod'] = ProductSerializer(instance.prod_id).data['name']
        # data['app_hosts'] = Hostserializer(instance.host_id).data
        return data
        

class ProductSerializer(serializers.ModelSerializer):
    """ 应用信息 """
    class Meta:
        model = Product
        fields = "__all__"

class EnvConfigSerializer(serializers.ModelSerializer):
    """ 环境配置 """
    class Meta:
        model = EnvConfig
        fields = "__all__"
        depth = 1

    def to_representation(self, instance):
        """自定义字段返回 """
        data = super().to_representation(instance)
        data['env_display'] = instance.get_type_display()
        return data
        
       
    
    # def to_representation(self, instance):
    #     """自定义字段返回 """
    #     data = super().to_representation(instance)

    #     data['env_choices_display'] = instance.env_choices_display()
    #     return data
        

    

class AppDeveloperSerializer(serializers.ModelSerializer):
    """ 开发者 """
    class Meta:
        model = AppDeveloper
        fields = "__all__"
