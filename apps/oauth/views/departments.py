from rest_framework.mixins import CreateModelMixin,ListModelMixin,RetrieveModelMixin, DestroyModelMixin,UpdateModelMixin
from rest_framework.generics import GenericAPIView

from rest_framework_jwt.authentication import JSONWebTokenAuthentication  # jwt用户认证
from rest_framework.permissions import IsAuthenticated
from rest_framework.filters import SearchFilter, OrderingFilter
from oauth.models import *
from api.serializers.departments import *
from rest_framework.viewsets import ModelViewSet
from utils.Mixins import  MultipleDestroyMixin
from utils.views import  TreeAPIView


class DepartmentsViewSet(ModelViewSet,TreeAPIView, MultipleDestroyMixin):
    """
    create:
    部门--新增

    部门新增, status: 201(成功), return: 新增部门信息

    destroy:
    部门--删除

    部门删除, status: 204(成功), return: None

    multiple_delete:
    部门--批量删除

    部门批量删除, status: 204(成功), return: None

    update:
    部门--修改

    部门修改, status: 200(成功), return: 修改增部门信息

    partial_update:
    部门--局部修改

    部门局部修改, status: 200(成功), return: 修改增部门信息

    list:
    部门--获取列表

    部门列表信息, status: 200(成功), return: 部门信息列表
    """
    queryset = Departments.objects.all()
    serializer_class = DepartmentsSerializer
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ['name']
    ordering_fields = ['id', 'name']

    def get_serializer_class(self):
        if self.action == 'list':
            return DepartmentsTreeSerializer
        else:
            return DepartmentsSerializer