#!/usr/bin/env python
# coding=utf-8
'''
author: 以谁为师
site: opsbase.cn
Date: 2021-12-01 05:02:26
LastEditTime: 2022-03-13 23:31:17
Description: 
'''
from django.apps import AppConfig

# class OauthConfig(AppConfig):
class UserProfileConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'oauth'

    def ready(self):
        import oauth.signals
        

    
