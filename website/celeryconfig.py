import os
from django.conf import settings

# 设置系统环境变量,安装django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'website.settings')

REDIS_CONN = f'redis://:{settings.REDIS_PWD}@{settings.REDIS_HOST}:{settings.REDIS_PORT}'
# print("REDIS_CONN:",REDIS_CONN)

# 消息broker
broker_url = f"{REDIS_CONN}/8"
# 存储任务状态及结果 django存储/redis
# result_backend = "django-db"
result_backend = f"{REDIS_CONN}/9"